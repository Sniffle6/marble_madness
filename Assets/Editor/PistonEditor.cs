﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Reflection;

[CanEditMultipleObjects]
[CustomEditor(typeof(Piston))]
public class PistonEditor : Editor {

    Piston m_Target;

    public override void OnInspectorGUI()
    {
        m_Target = (Piston)target;

        DrawDefaultInspector();
        DrawStatesInspector();
    }

    void DrawStatesInspector()
    {
        GUILayout.Space(5);
        GUILayout.Label("States", EditorStyles.boldLabel);

        for (int i = 0; i < m_Target.States.Count; i++)
        {
            DrawState(i);
        }
        DrawAddStateButton();
    }
    void DrawState(int index)
    {
        if(index < 0 || index >= m_Target.States.Count)
        {
            return;
        }

        SerializedProperty listIterator = serializedObject.FindProperty("States");
        GUILayout.BeginHorizontal();
        {
            //If this object is an instantiated prefab, we want to mirror Unitys default inspector behaviour where
            //variables that have been modified (but not applied) are drawn in bold text
            if (listIterator.isInstantiatedPrefab == true)
            {
                //The SetBoldDefaultFont functionality is usually hidden from us but we can use some tricks to
                //access the method anyways. See the implementation of our own EditorGUIHelper.SetBoldDefaultFont
                //for more info
                EditorGUIHelper.SetBoldDefaultFont(listIterator.GetArrayElementAtIndex(index).prefabOverride);
            }
            GUILayout.Label("Name", EditorStyles.label, GUILayout.Width(50));

            EditorGUI.BeginChangeCheck();
            string newName = GUILayout.TextField(m_Target.States[index].Name, GUILayout.Width(120));
            Vector3 newPosition = EditorGUILayout.Vector3Field("", m_Target.States[index].Position);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(m_Target, "Modify State");
                m_Target.States[index].Name = newName;
                m_Target.States[index].Position = newPosition;
                EditorUtility.SetDirty(m_Target);
            }

            EditorGUIHelper.SetBoldDefaultFont(false);

            if (GUILayout.Button("Remove"))
            {
                EditorApplication.Beep();

                if (EditorUtility.DisplayDialog("Really?", "Do you really want to remove the state '" + m_Target.States[index].Name + "'?", "Yes", "No") == true)
                {
                    Undo.RecordObject(m_Target, "Delete State");
                    m_Target.States.RemoveAt(index);
                    EditorUtility.SetDirty(m_Target);
                }
            }
        }
        GUILayout.EndHorizontal();
    }
    void DrawAddStateButton()
    {
        if(GUILayout.Button("Add new State", GUILayout.Height(30)))
        {
            Undo.RecordObject(m_Target, "Add new State");
            m_Target.States.Add(new PistonState { Name = "New State" });
            EditorUtility.SetDirty(m_Target);
        }
    }
}
