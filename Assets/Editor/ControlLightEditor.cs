﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ControlLight), true)]
public class ControlLightEditor : Editor
{
    ControlLight m_Target;

    public override void OnInspectorGUI()
    {
        m_Target = (ControlLight)target;
        DrawDefaultInspector();

        if (!Application.isPlaying)
        {
            m_Target.rend.sharedMaterial.EnableKeyword("_EMISSION");
            m_Target.rend.sharedMaterial.SetColor("_EmissionColor", m_Target.rend.sharedMaterial.color * m_Target.controlValue * m_Target.emissionPercent);
            m_Target.pointLight.range = m_Target.controlValue;
            // pointLight.intensity = controlValue * 0.3f;
        }
    }
}
