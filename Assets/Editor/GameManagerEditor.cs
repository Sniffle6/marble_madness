﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{

    GameManager m_Target;
    CheckpointData data;
    public void OnEnable()
    {
        m_Target = target as GameManager;
        data = new CheckpointData();
        m_Target.p_checkpointData = JsonController.LoadData<CheckpointData>(Application.streamingAssetsPath + "/Checkpoint.json") as CheckpointData;
    }
    public override void OnInspectorGUI()
    {
        m_Target = (GameManager)target;
        DrawDefaultInspector();
        if (!Application.isPlaying)
        {
            if (GUILayout.Button("Reset Player Data"))
            {
                data.SpawnSpot = GameObject.FindGameObjectWithTag("Respawn").transform.position;
                m_Target.p_checkpointData = data;
                DatController.Save(data, "Checkpoint");
            }
        }
    }
}
