﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Item))]
public class ItemEditor : Editor
{
    Item m_Target;
    private void OnEnable()
    {
        m_Target = (Item)target;
        CheckAndAddChild();
    }
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CheckAndAddChild();
    }
    void CheckAndAddChild()
    {
        int childCount = m_Target.transform.childCount;
        var e = Event.current;
        if (e != null && e.type == EventType.Used && e.commandName == "ObjectSelectorUpdated")
        {
            var c = m_Target.transform.GetComponentsInChildren<Renderer>();
            for (int i = 0; i < c.Length; i++)
            {
                DestroyImmediate(c[i].gameObject);
            }
            if (m_Target._data)
            {
                if (m_Target._data._graphics)
                {
                    var model = Instantiate(m_Target._data._graphics);
                    model.transform.parent = m_Target.transform;
                    model.transform.localPosition = Vector3.zero;
                }
            }
        }
    }
    private void OnDisable()
    {
        if (!m_Target._data)
        {
            throw new System.Exception("Item not set on script!");
        }
        else
        {
            if (!m_Target._data._graphics)
            {
                throw new System.Exception("Graphics not set on item scriptable object!");
            }
        }

    }
}

