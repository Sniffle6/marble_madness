﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/DirectionalLight"
{
	Properties
	{
		_BaseColor("Base Color", Color) = (1,0,0,1)
		_UpColor("UpColor", Color) = (1,0,0,1)
		_RightColor("RightColor", Color) = (0,0,0,1)
		_FrontColor("FrontColor", Color) = (0,0,0,1)

	}
		SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 100

		Pass
	{
		CGPROGRAM
	#pragma vertex vert
	#pragma fragment frag
		//makefog work
	#pragma multi_compile_fog

	#include "UnityCG.cginc"

		struct appdata
	{
		float4 vertex : POSITION;
		float4 normal : NORMAL;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 color : COLOR;
		UNITY_FOG_COORDS(1)
			float4 vertex : SV_POSITION;
	};
	

	float4 _BaseColor;
	float4 _UpColor;
	float4 _RightColor;
	float4 _FrontColor;


	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		UNITY_TRANSFER_FOG(o,o.vertex);
		float3 normal = UnityObjectToWorldNormal(v.normal);
		o.color = _BaseColor/3;
		o.color += _RightColor * saturate(dot(normal, float3(-1,0,0)));
		o.color +=  _UpColor * saturate(dot(normal, float3(0,1,0)));
		o.color += _FrontColor* saturate(dot(normal, float3(0, 0, -1)));
		return o;
	}


	fixed4 frag(v2f i) : SV_Target
	{
		fixed4 col = i.color;
	//apply fog
	UNITY_APPLY_FOG(i.fogCoord, col);
	return col;
	}
	ENDCG
	}
	}
}

