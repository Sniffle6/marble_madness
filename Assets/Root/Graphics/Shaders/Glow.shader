﻿Shader "Custom/Glow" {
	Properties {
		_Color("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
	_BumpMap("Normal Map", 2D) = "bump" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_RimColor("Rim Color", Color) = (1,1,1,1)
		_RimPower("Rim Power", Range(1.0,6.0)) = 3.0


			_BaseColor("Base Color", Color) = (1,0,0,1)
			_UpColor("UpColor", Color) = (1,0,0,1)
			_RightColor("RightColor", Color) = (0,0,0,1)
			_FrontColor("FrontColor", Color) = (0,0,0,1)

	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		struct Input {
			float4 color:Color;
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float3 viewDir;
		};

		half _Glossiness;
		half _Metallic;
		float4 _Color;
		sampler2D _MainTex;
		sampler2D _BumpMap;
		float4 _RimColor;
		float _RimPower;


		float4 _BaseColor;
		float4 _UpColor;
		float4 _RightColor;
		float4 _FrontColor;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			IN.color = _BaseColor / 3;
			IN.color += _RightColor * saturate(dot(IN.viewDir, float3(0,1, 0)));
			IN.color += _UpColor * saturate(dot(IN.viewDir, float3(1, 0, 0)));
			IN.color += _FrontColor* saturate(dot(IN.viewDir, float3(0, 0, 1)));

			//IN.color = _Color;
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb * IN.color;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
			half rim = 1 - saturate(dot(normalize(IN.viewDir), o.Normal));
			o.Emission = _RimColor.rgb  * pow(rim, _RimPower);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
