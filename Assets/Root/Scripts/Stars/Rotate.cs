﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
    Vector3 rotation;
    float speed;
	// Use this for initialization
	void Start () {
        rotation = new Vector3(Random.Range(-1, 2), Random.Range(-1, 2), Random.Range(-1, 2));
        if (rotation == Vector3.zero)
            rotation = Vector3.right;

        speed = Random.Range(10, 100);
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(rotation * Time.deltaTime* speed, Space.World);
	}
}
