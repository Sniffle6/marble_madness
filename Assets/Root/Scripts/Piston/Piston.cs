﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Piston : MonoBehaviour
{

    public float Speed;
    public Vector3 AddForceWhenHittingPlayer;

    [HideInInspector]
    public List<PistonState> States = new List<PistonState>();


    Transform m_Visuals;
    Transform Visuals
    {
        get
        {
            if (m_Visuals)
            {
                return m_Visuals;
            }
            else
                return m_Visuals = transform.GetChild(0);
        }
    }
    Vector3 m_VisualsTargetPosition;
    float startTime;
    float m_MovingSpeed = 0;
    float journeyLength = 0;
    Vector3 m_MovingDirection;

    void Awake()
    {
        m_VisualsTargetPosition = Visuals.transform.localPosition;
        startTime = Time.time;
        m_MovingSpeed = 0;
        journeyLength = 1;
    }

    void Update()
    {
        UpdateVisualsPosition();
    }
    void UpdateVisualsPosition()
    {
        var v_localPosition = Visuals.transform.localPosition;

        float distCovered = (Time.time - startTime) * Speed;
        float fracJourney = distCovered / journeyLength;

        Vector3 newPosition = Vector3.Lerp(v_localPosition, m_VisualsTargetPosition, fracJourney);

        m_MovingSpeed = Vector3.Distance(v_localPosition, newPosition) / Time.deltaTime;
        if (m_MovingSpeed == 0)
        {
            m_MovingDirection = Vector3.zero;
        }
        else
        {
            m_MovingDirection = (newPosition - v_localPosition).normalized;
        }
        Visuals.transform.localPosition = newPosition;
    }

    public void SetState(string state)
    {
        for (int i = 0; i < States.Count; i++)
        {
            if (States[i].Name == state)
            {
                SetTargetPosition(States[i].Position);
                startTime = Time.time;
                journeyLength = Vector3.Distance(Visuals.transform.localPosition, m_VisualsTargetPosition);
                return;
            }
        }
        Debug.LogWarning("Could not find PistonState '" + state + "'");
    }
    public float GetMovingSpeed()
    {
        return m_MovingSpeed;
    }
    public Vector3 GetMovingDirection()
    {
        return m_MovingDirection;
    }
    void SetTargetPosition(float x, float y, float z)
    {
        SetTargetPosition(new Vector3(x, y, z));
    }

    void SetTargetPosition(Vector3 position)
    {
        m_VisualsTargetPosition = position;

        if (Application.isPlaying == false)
        {
            Visuals.transform.position = m_VisualsTargetPosition;
        }
    }
}
