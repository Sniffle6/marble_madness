﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Ball;

//Helper class that defines what happens if the marble collides with this
public class PistonCollision : MonoBehaviour
{
    public float HitForce;

    Piston m_Piston;

    void Awake()
    {
        m_Piston = GetComponentInParent<Piston>();
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.GetComponent<Ball>())
        {
            collision.gameObject.GetComponent<Ball>().inPistonTrigger = true;
            collision.gameObject.GetComponent<Ball>().SlowDownCounter = 0;
        }
        if (collision.collider.tag != "Player")
        {
            return;
        }

        if (m_Piston.GetMovingSpeed() < 0.001f)
        {
            return;
        }

        if (Vector3.Dot(collision.contacts[0].normal, m_Piston.GetMovingDirection()) > -0.5f)
        {
            return;
        }
        collision.rigidbody.velocity = m_Piston.GetMovingDirection() * HitForce + m_Piston.AddForceWhenHittingPlayer;
    }
}
