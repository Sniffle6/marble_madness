﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistonLinkedTrigger : MonoBehaviour {

    Piston m_Piston;
    void Awake()
    {
        m_Piston = GetComponent<Piston>();
    }
    public void InitializeMove(string state)
    {
        m_Piston.SetState(state);
    }
}
