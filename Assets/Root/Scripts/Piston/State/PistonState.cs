﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class PistonState  {
    public string Name;
    public Vector3 Position;
}
