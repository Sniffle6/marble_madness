﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Ball;

public class PistonTrigger : MonoBehaviour
{

    public string StateOnEnter;
    public string StateOnExit;

    Piston m_Piston;

    private Vector3 m_colliderPosition = new Vector3();
    private Vector3 m_PistonPosition = new Vector3();
    void Awake()
    {
        m_Piston = GetComponent<Piston>();
    }

    void OnTriggerEnter(Collider collider)
    {
        if (gameObject.tag != "PistonFind" && collider.tag == "Enemy")
        {
            m_Piston.SetState(StateOnEnter);
        }
        if (collider.tag == "Player")
        {
            collider.gameObject.GetComponent<Ball>().inPistonTrigger = true;
            collider.gameObject.GetComponent<Ball>().SlowDownCounter = 0;
            if (gameObject.tag != "PistonFind")
            {
                m_Piston.SetState(StateOnEnter);
            }
            else
            {
                m_colliderPosition = collider.transform.position;
                m_PistonPosition = m_Piston.transform.position;
                if (Mathf.Abs(collider.GetComponent<Ball>().m_moveDirection.x) > 0 && Mathf.Abs(collider.GetComponent<Ball>().m_moveDirection.z) < 0.5f)
                {
                    if (m_colliderPosition.z > m_PistonPosition.z + 0.5f)
                    {
                        m_Piston.AddForceWhenHittingPlayer = new Vector3(0, 0, 4);
                        m_Piston.SetState("Left");
                    }
                    else if (m_colliderPosition.z < m_PistonPosition.z - 0.5f)
                    {
                        m_Piston.AddForceWhenHittingPlayer = new Vector3(0, 0, -4);
                        m_Piston.SetState("Right");
                    }
                    else
                    {
                        if (m_colliderPosition.x > m_PistonPosition.x)
                        {
                            m_Piston.AddForceWhenHittingPlayer = new Vector3(4, 0, 0);
                            m_Piston.SetState("Forward");
                        }
                        else
                        {
                            m_Piston.AddForceWhenHittingPlayer = new Vector3(-4, 0, 0);
                            m_Piston.SetState("Back");
                        }
                    }
                }
                else if (Mathf.Abs(collider.GetComponent<Ball>().m_moveDirection.z) > 0 && Mathf.Abs(collider.GetComponent<Ball>().m_moveDirection.x) < 0.5f)
                {
                    if (m_colliderPosition.x > m_PistonPosition.x + 0.5f)
                    {
                        m_Piston.AddForceWhenHittingPlayer = new Vector3(4, 0, 0);
                        m_Piston.SetState("Forward");
                    }
                    else if (m_colliderPosition.x < m_PistonPosition.x - 0.5f)
                    {
                        m_Piston.AddForceWhenHittingPlayer = new Vector3(-4, 0, 0);
                        m_Piston.SetState("Back");
                    }else
                    {
                        if (m_colliderPosition.z > m_PistonPosition.z)
                        {
                            m_Piston.AddForceWhenHittingPlayer = new Vector3(0, 0, 4);
                            m_Piston.SetState("Left");
                        }
                        else
                        {
                            m_Piston.AddForceWhenHittingPlayer = new Vector3(0, 0, -4);
                            m_Piston.SetState("Right");
                        }
                    }
                }
                
            }
        }
    }
    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player" || collider.tag == "Enemy")
        {
            m_Piston.SetState(StateOnExit);
        }
    }

}
