﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DetectPlayer : MonoBehaviour
{

    public bool playerDetected;
    GameObject m_Player;
    ControlLight m_PlayerControlLight;
    Light playerLight;
    Light thisLight;
    int ignoreRayLayer = 13;
    int ignoreRayLayerMask;
    public float visionRangeCap;
    [HideInInspector]
    public float maxVisionRange;
    NavMeshAgent _agent;
    [Header("Max Range Based Off Player Light")]
    [Tooltip("This Number * the player light range = the enemy max vision range!")]
    public float enemyMaxrangeModifier;
    // Use this for initialization
    void Start()
    {
        m_Player = GameObject.FindGameObjectWithTag("Player");
        m_PlayerControlLight = m_Player.GetComponent<ControlLight>();
        playerLight = m_Player.GetComponentInChildren<Light>();
        ignoreRayLayerMask = 1 << ignoreRayLayer;
        ignoreRayLayerMask = ~ignoreRayLayerMask;
        _agent = GetComponent<NavMeshAgent>();
        thisLight = GetComponentInChildren<Light>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        var playerPositionThisFrame = m_Player.transform.position;
        var thisPositionThisFrame = transform.position;
        PlayerDetector(playerPositionThisFrame, thisPositionThisFrame);
        if (playerDetected)
        {
            _agent.destination = playerPositionThisFrame;
        }
        Debug.DrawLine(thisPositionThisFrame, playerPositionThisFrame);
    }
    void PlayerDetector(Vector3 playerPosition, Vector3 myPos)
    {
        if (Vector3.Distance(myPos, playerPosition) < maxVisionRange)
        {
            RaycastHit hit;
            if (Physics.Linecast(myPos, playerPosition, out hit, ignoreRayLayerMask))
            {
                if (hit.collider.CompareTag("Player"))
                {
                    if (Vector3.Distance(myPos, playerPosition) - 0.25f < thisLight.range)
                    {
                        playerDetected = true;
                        return;
                    }
                    if (playerLight.range > 0 && playerLight.intensity > 0)
                    {
                        playerDetected = true;

                    }
                    else
                    {
                        playerDetected = false;
                    }
                }
                else
                {
                    playerDetected = false;
                    if (Vector3.Distance(myPos, playerPosition) - 0.25f < m_PlayerControlLight.pointLight.range / 2 && _agent.pathStatus == NavMeshPathStatus.PathComplete && _agent.remainingDistance <= 0.5f)
                    {
                        _agent.destination = playerPosition;
                    }
                }
            }
        }
        else
        {
            playerDetected = false;
        }
    }
    private void Update()
    {

        maxVisionRange = m_PlayerControlLight.pointLight.range * enemyMaxrangeModifier;
        if (maxVisionRange > visionRangeCap)
            maxVisionRange = visionRangeCap;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        if (Application.isPlaying)
        {
            Gizmos.DrawWireSphere(transform.position, maxVisionRange);
        }
        else
        {
            Gizmos.DrawWireSphere(transform.position, visionRangeCap);
        }
    }
}
