﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public bool useCheckpoint;
    public static GameManager instance = null;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public GameObject m_Player;
    public CheckpointData p_checkpointData;
    public CheckpointData p_lastSavedCheckpointData;
    [HideInInspector]
    public Vector3 respawnLoc;
    int firstRun = 0;
    CheckpointData SaveData;
    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }
    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.name.Contains("Level") && m_Player)
        {
            GameObject rspwn = GameObject.FindGameObjectWithTag("Respawn");
            m_Player.transform.position = rspwn.transform.position;
            Camera.main.transform.position = rspwn.transform.position + new Vector3(-12f, 7f, -6f);
            var scenes = SceneManager.sceneCount;
            bool found = false;
            for (var i = 0; i < scenes; i++)
            {
                if (SceneManager.GetSceneAt(i).name == "UI")
                    found = true;
            }
            if (!found)
                SceneManager.LoadSceneAsync(0, LoadSceneMode.Additive);
            m_Player.GetComponent<DetectDeath>().respawnLocation = rspwn;
            SaveData.SpawnSpot = rspwn.transform.position;
            DatController.Save(SaveData, "Checkpoint");
        }
        Debug.Log("Level Loaded");
        Debug.Log(scene.name);
        Debug.Log(mode);
    }
    void Start()
    {
        // if ((CheckpointData)DatController.Load<CheckpointData>("Checkpoint") != null)
        // {
        p_checkpointData = (CheckpointData)DatController.Load<CheckpointData>("Checkpoint");
        p_lastSavedCheckpointData = p_checkpointData;
        // }
        //p_checkpointData = JsonController.LoadData<CheckpointData>(Application.streamingAssetsPath + "/Checkpoint.json") as CheckpointData;
        //firstRun = PlayerPrefs.GetInt("savedFirstRun");
        //if (firstRun == 0)
        //{
        //    print("first Run");
        //    p_checkpointData.SpawnSpot = Vector3.zero;
        //    JsonController.SaveData(p_checkpointData, Application.streamingAssetsPath + "/Checkpoint.json");
        //    firstRun = 1;
        //    PlayerPrefs.SetInt("savedFirstRun", firstRun);
        //}
        if (!GameObject.Find("Respawn"))
        {
            Debug.LogError("There is no respawn in the scene. Incoming erros, Sniffle6 says, add a respawn location!");
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
        }
        respawnLoc = GameObject.Find("Respawn").transform.position;
        if (!m_Player)
        {
            m_Player = GameObject.FindGameObjectWithTag("Player");
        }
        if (!m_Player)
        {
            m_Player = (GameObject)Instantiate(Resources.Load("Player"));
        }
        if (m_Player)
        {
            if (useCheckpoint)
                MovePlayerToLastCheckPoint();
            else
            {
                m_Player.transform.position = GameObject.FindGameObjectWithTag("Respawn").transform.position;
            }
        }
        var scenes = SceneManager.sceneCount;
        bool found = false;
        for (var i = 0; i < scenes; i++)
        {
            if (SceneManager.GetSceneAt(i).name == "UI")
                found = true;
        }
        if (!found)
            SceneManager.LoadSceneAsync(0, LoadSceneMode.Additive);
    }
    public void MovePlayerToLastCheckPoint()
    {
        if (p_checkpointData != null)
        {
            //  print(p_checkpointData.SpawnSpot);
            m_Player.transform.position = p_checkpointData.SpawnSpot;
           // Camera.main.transform.position = new Vector3(p_checkpointData.SpawnSpot.x, Camera.main.transform.position.y, p_checkpointData.SpawnSpot.z);
        }
        else
        {
            m_Player.transform.position = respawnLoc;
           // Camera.main.transform.position = new Vector3(respawnLoc.x, Camera.main.transform.position.y, respawnLoc.z);
            print("no data in checkpoint data");
        }
    }
}
