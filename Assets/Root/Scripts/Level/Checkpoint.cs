﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public Transform _spawnSpot;
    CheckpointData data;
    private void Start()
    {
        data = GameManager.instance.p_checkpointData;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (data == null)
            {
                data = new CheckpointData();
            }
            if (!Approximately(data.SpawnSpot, _spawnSpot.position))
            {
                data.SpawnSpot = _spawnSpot.position;
                DatController.Save(data, "Checkpoint");
                GameManager.instance.p_checkpointData = data;
            }
            // JsonController.SaveData(data, Application.streamingAssetsPath + "/Checkpoint.json");
        }
    }
    public bool Approximately(Vector3 me, Vector3 other)
    {
        if (me.x == other.x && me.y == other.y && me.z == other.z)
            return true;
        else
            return false;
    }
}
