﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Rotation : MonoBehaviour
{

    //protected Transform movingHandle;
   // public List<Vector3> states = new List<Vector3>();
    //private Quaternion[] rotations;
    //public int currentState;
    //public Quaternion openRotation = Quaternion.Euler(0, 45, 0);
    //public Quaternion closeRotation = Quaternion.Euler(0, 0, 0);

    // Use this for initialization
   // void Start()
   // {
      //  rotations = new Quaternion[states.Count];
       // for (int i = 0; i < rotations.Length; i++)
       // {
      //      rotations[i] = Quaternion.Euler(states[i]);
       // }
       // currentState = 0;
       // movingHandle = gameObject.transform;
       // PlayCurrentState();
   // }
    //public void NextState()
    //{
    //    if (currentState < states.Count - 1)
    //        currentState += 1;
    //}
    //public void LastState()
    //{
    //    currentState -= 1;
    //}
    //public void PlayNextState()
    //{
    //    RotateOverTime(transform.rotation, rotations[currentState += 1], 1);
    //}
    //public void PlayCurrentState()
    //{
    //    if (rotations[currentState] != null)
    //        StartCoroutine(RotateOverTime(transform.rotation, rotations[currentState], 1));
    //}
    //public void PlayPreviousState()
    //{
    //    currentState -= 1;
    //    RotateOverTime(transform.rotation, rotations[currentState -= 1], 1);
    //}
    //IEnumerator RotateOverTime(Quaternion originalRotation, Quaternion finalRotation, float duration)
    //{
    //    if (duration > 0f)
    //    {
    //        float startTime = Time.time;
    //        float endTime = startTime + duration;
    //        movingHandle.transform.rotation = originalRotation;
    //        yield return null;
    //        while (Time.time < endTime)
    //        {
    //            float progress = (Time.time - startTime) / duration;
    //            // progress will equal 0 at startTime, 1 at endTime.
    //            movingHandle.transform.rotation = Quaternion.Lerp(originalRotation, finalRotation, progress);
    //            yield return null;
    //        }
    //    }
    //    NextState();
    //    PlayCurrentState();
    //    movingHandle.transform.rotation = finalRotation;
    //}




    public float Speed;
    public Vector3 AddForceWhenHittingPlayer;

    [HideInInspector]
    public List<PistonState> States = new List<PistonState>();


    Transform m_Visuals;
    Transform Visuals
    {
        get
        {
            if (m_Visuals)
            {
                return m_Visuals;
            }
            else
                return m_Visuals = transform.GetChild(0);
        }
    }
    Quaternion m_VisualsTargetRotation;
    float startTime;
    float m_MovingSpeed = 0;
    float journeyLength = 0;

    void Awake()
    {
        //rotations = new Quaternion[States.Count];
        //for (int i = 0; i < rotations.Length; i++)
        //{
        //    rotations[i] = Quaternion.Euler(States[i].Position);
        //}

        m_VisualsTargetRotation = Visuals.transform.localRotation;
       // startTime = Time.time;
      //  m_MovingSpeed = 0;
      //  journeyLength = 1;
    }

    IEnumerator RotateOverTime(Quaternion originalRotation, Quaternion finalRotation, float duration)
    {
        if (duration > 0f)
        {
            float startTime = Time.time;
            float endTime = startTime + duration;
            Visuals.rotation = originalRotation;
            yield return null;
            while (Time.time < endTime)
            {
                float progress = (Time.time - startTime) / duration;
                // progress will equal 0 at startTime, 1 at endTime.
                Visuals.rotation = Quaternion.Lerp(originalRotation, finalRotation, progress);
                yield return null;
            }
        }
        // NextState();
        //  PlayCurrentState();
        Visuals.rotation = finalRotation;
    }

    public void SetState(string state)
    {
        for (int i = 0; i < States.Count; i++)
        {
            if (States[i].Name == state)
            {
                SetTargetRotations(Quaternion.Euler(States[i].Position));
                startTime = Time.time;
                //journeyLength = Vector3.Distance(Visuals.transform.localPosition, m_VisualsTargetPosition);
                StartCoroutine(RotateOverTime(Visuals.rotation, m_VisualsTargetRotation, Speed));
                return;
            }
        }
        Debug.LogWarning("Could not find RotationState '" + state + "'");
    }
    public float GetMovingSpeed()
    {
        return m_MovingSpeed;
    }

    //void SetTargetRotation(float x, float y, float z)
    //{
    //    SetTargetEulers(new Vector3(x, y, z));
    //}

    void SetTargetRotations(Quaternion rot)
    {
        m_VisualsTargetRotation = rot;

        if (Application.isPlaying == false)
        {
            Visuals.transform.localRotation = m_VisualsTargetRotation;
        }
    }
}
