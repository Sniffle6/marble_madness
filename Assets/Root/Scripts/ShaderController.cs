﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Ball;
public class ShaderController : MonoBehaviour
{


    float holdGPUMeltData = -1000;
    float holdGPUSidePick = 0;
   // float holdGPUMeltDistance = 0;
    private GameObject lastObjHit;
    private Renderer rend;
    private Material mat;
   // Transform trans;
    Ball ball;
    // Use this for initialization
    void Start()
    {
        rend = transform.GetChild(1).GetComponent<Renderer>();
        mat = rend.material;
        ball = GetComponent<Ball>();
      //  trans = gameObject.transform;
    }
    void Update()
    {
        if (lastObjHit)
        {
            if (!ball.onGround && Vector3.Distance(lastObjHit.transform.position, gameObject.transform.position) > 20)
            {
                holdGPUSidePick = 0;
                mat.SetFloat("_PickSide", holdGPUSidePick);
                holdGPUMeltData = -900;
                mat.SetFloat("_MeltY", holdGPUMeltData);
            }
        }
    }
    void OnCollisionExit(Collision other)
    {
    //    if (!ball.onGround && trans.position.y < holdGPUMeltData + 0.4f && other.gameObject.tag == "Floor" && holdGPUMeltData != -900 && holdGPUSidePick != 1 && holdGPUSidePick != 2)
    //    {
    //        holdGPUMeltData = -900;
    //        mat.SetFloat("_MeltY", holdGPUMeltData);
    //    }
    }
    void OnCollisionEnter(Collision other)
    {
        if (holdGPUMeltData != other.transform.position.y )
        {
            if (other.gameObject.tag == "Floor")
            {
                print("entered floor");
                holdGPUMeltData = other.transform.position.y;
                mat.SetFloat("_MeltY", holdGPUMeltData);
                holdGPUSidePick = 0;
                mat.SetFloat("_PickSide", holdGPUSidePick);
                if (lastObjHit != other.gameObject)
                    lastObjHit = other.gameObject;
            }
            else if (other.gameObject.tag == "RightFloor")
            {
                holdGPUSidePick = 1;
                mat.SetFloat("_PickSide", holdGPUSidePick);
                holdGPUMeltData = other.transform.position.z;
                mat.SetFloat("_MeltY", holdGPUMeltData);
                if (lastObjHit != other.gameObject)
                    lastObjHit = other.gameObject;
            }
            else if (other.gameObject.tag == "BackFloor")
            {
                holdGPUSidePick = 2;
                mat.SetFloat("_PickSide", holdGPUSidePick);
                holdGPUMeltData = other.transform.position.x;
                mat.SetFloat("_MeltY", holdGPUMeltData);
                if (lastObjHit != other.gameObject)
                    lastObjHit = other.gameObject;
            }
        }
        //holdGPUMeltDistance = 0;
       // holdGPUMeltDistance = Mathf.Lerp(holdGPUMeltDistance, 1, Time.deltaTime * 5);
       // mat.SetFloat("_MeltDistance", holdGPUMeltDistance);
    }
}
