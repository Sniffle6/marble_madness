﻿using UnityEngine;
using DigitalRuby.SoundManagerNamespace;
using UnityEngine.UI;

public class HandleSounds : MonoBehaviour {
    

    public AudioSource[] SoundAudioSources;
    public AudioSource[] MusicAudioSources;

    public static HandleSounds instance = null;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            if (instance != this)
                Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void PlaySound(int index)
    {
        int count;
        if (!int.TryParse("1", out count))
        {
            count = 1;
        }
        while (count-- > 0)
        {
            if (!SoundAudioSources[index])
                return;
            SoundAudioSources[index].pitch = Random.Range(0.7f, 1.4f);
            SoundAudioSources[index].PlayOneShotSoundManaged(SoundAudioSources[index].clip);
        }
    }

    private void PlayMusic(int index)
    {
        MusicAudioSources[index].PlayLoopingMusicManaged(1.0f, 1.0f, true);
    }

    private void CheckPlayKey()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.LogWarning("Reloading level");
            UnityEngine.SceneManagement.SceneManager.LoadScene(0, UnityEngine.SceneManagement.LoadSceneMode.Single);
        }
    }
     

    private void Start()
    {

    }

    private void Update()
    {
        CheckPlayKey();
    }
}

