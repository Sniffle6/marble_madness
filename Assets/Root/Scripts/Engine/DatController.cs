﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DatController
{

    public static void Save(object objectToSave, string fileName)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file;
        file = File.Open(Application.persistentDataPath + "/" + fileName + ".dat", FileMode.Create);

        bf.Serialize(file, objectToSave);
        file.Close();
    }
    public static object Load<FileType>(string fileName)
    {
        if (File.Exists(Application.persistentDataPath + "/" + fileName + ".dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + fileName + ".dat", FileMode.Open);
            FileType data = (FileType)bf.Deserialize(file);
            file.Close();
            Debug.Log(Application.persistentDataPath + "/" + fileName + ".dat");
            return data;
        }
        return null;
    }
}

[Serializable]
public class PlayerData
{
    [SerializeField]
    public Vector3 LastCheckpointSaved;
}
