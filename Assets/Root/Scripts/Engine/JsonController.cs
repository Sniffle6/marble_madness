﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public static class JsonController  {

    public static object LoadData<T>(string path)
    {
        string jsonString = File.ReadAllText(path);
        T FileType = JsonUtility.FromJson<T>(jsonString);
        return FileType;
    }
    public static void SaveData(object objtoSave, string path)
    {
        string newData = JsonUtility.ToJson(objtoSave);
        File.WriteAllText(path, newData);
    }




}

[System.Serializable]
public class CheckpointData
{
    public SerializableVector3 SpawnSpot;
}
