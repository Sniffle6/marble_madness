﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ControlLight : MonoBehaviour
{
    public Renderer rend;
    [HideInInspector]
    public Material mat;
    public Light pointLight;
    [Range(0, 10)]
    public float controlValue = -1;
    float controlValueLastFrame = -2;
    [Range(0, 1)]
    public float emissionPercent;
    [HideInInspector]
    public Color startColor;
   // float randomMod;
    float counter;
    // Use this for initialization
    void Start()
    {
        mat = rend.material;
        startColor = mat.color;
        //randomMod = Random.Range(6, 10);
        counter = Random.Range(0, 1000);
        ExtendStart();

    }
    public abstract void ExtendStart();

    // Update is called once per frame
    void Update()
    {
        counter+= Time.deltaTime;
        // controlValue = Mathf.PingPong(counter * randomMod, 10f);
        AdjustLight();
        ExtendUpdate();
    }
    void AdjustLight()
    {
        if (controlValue != controlValueLastFrame)
        {
            // print("running");
            mat.EnableKeyword("_EMISSION");
            mat.SetColor("_EmissionColor", startColor * controlValue * emissionPercent);
            pointLight.range = controlValue;
            // pointLight.intensity = controlValue * 0.3f;
        }
    }
    public abstract void ExtendUpdate();
    private void LateUpdate()
    {
        controlValueLastFrame = controlValue;
    }
}
