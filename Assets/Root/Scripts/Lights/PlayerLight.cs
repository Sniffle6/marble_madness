﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLight : ControlLight
{

    Slider slider;
    public override void ExtendStart()
    {
        if (GameObject.Find("LightAdjust"))
            slider = GameObject.Find("LightAdjust").GetComponent<Slider>();
    }

    public override void ExtendUpdate()
    {
        if (!slider)
        {
            if (GameObject.Find("LightAdjust"))
            {
                slider = GameObject.Find("LightAdjust").GetComponent<Slider>();
                slider.onValueChanged.AddListener(AdjustControlValue);
                controlValue = slider.value;
            }
        }
    }
    public void AdjustControlValue(float value)
    {
        controlValue = value;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, pointLight.range / 2);
    }
}
