using System;
using UnityEngine;
using System.Collections.Generic;

namespace UnityStandardAssets.Vehicles.Ball
{
    public class Ball : MonoBehaviour
    {
        [SerializeField]
        private float m_MovePower = 5; // The force added to the ball to move it.
        [SerializeField]
        private bool m_UseTorque = true; // Whether or not to use torque to move the ball.
        [SerializeField]
        private float m_MaxAngularVelocity = 25; // The maximum velocity the ball can rotate at.
        [SerializeField]
        private float m_JumpPower = 2; // The force added to the ball when it jumps.

        private Rigidbody m_Rigidbody;
        private bool canJump;
        private bool hasJumped;
        public bool jump;
        public bool onGround;
        public Vector3 m_moveVelocity;
        public float m_moveSpeed;
        public Vector3 m_moveDirection = new Vector3();
        public bool inPistonTrigger = false;
        public float SlowDownCounter = 0f;
        //int framesParticlesHaveEmmited = 0;


        private void Start()
        {
            m_Rigidbody = GetComponent<Rigidbody>();
            // Set the maximum angular velocity.
            GetComponent<Rigidbody>().maxAngularVelocity = m_MaxAngularVelocity;
            canJump = true;


        }
        void OnCollisionExit(Collision other)
        {
            // GetComponentInChildren<ParticleSystem>().startSize = 0;
            onGround = false;
        }
        private void OnCollisionStay(Collision collision)
        {
            if (collision.contacts[0].point.y < transform.position.y)
                onGround = true;
            else
                onGround = false;
        }
        void OnCollisionEnter(Collision other)
        {
            if (other.contacts[0].point.y < transform.position.y)
                onGround = true;
            else
                onGround = false;
            if (hasJumped && !canJump && onGround)
            {
                onGround = true;
                canJump = true;
                jump = false;
                hasJumped = false;
            }
            //Camera.main.GetComponent<ScreenShake>().InitializeScreenShake(0, 0, m_moveSpeed / 100);
            HandleSounds.instance.PlaySound(0);

        }


        Vector3 positionLastFrame = new Vector3();
        public void Move(Vector3 moveDirection)
        {
            if (inPistonTrigger)
                SlowDownCounter++;
            if (SlowDownCounter > 120)
            {
                inPistonTrigger = false;
                SlowDownCounter = 0;
            }
            m_moveVelocity = GetComponent<Rigidbody>().velocity;
            m_moveSpeed = m_moveVelocity.magnitude;
            m_moveDirection = (transform.position - positionLastFrame).normalized;
            positionLastFrame = transform.position;
            if (moveDirection == Vector3.zero && onGround && !inPistonTrigger && m_moveSpeed != 0)
            {
                /* */
                m_Rigidbody.AddForce(m_moveDirection * -1);
                if (m_moveSpeed < 0.02f && m_moveSpeed > 0)
                {
                    m_Rigidbody.velocity = Vector3.zero;
                    m_moveVelocity = Vector3.zero;
                    m_moveDirection = Vector3.zero;
                    m_moveSpeed = 0;
                }
            }
            //Debug.Log(m_moveDirection);
            // If using torque to rotate the ball...
            if (m_UseTorque)
            {
                // ... add torque around the axis defined by the move direction.
                if (m_Rigidbody)
                    m_Rigidbody.AddTorque(new Vector3(moveDirection.z, 0, -moveDirection.x) * m_MovePower);
            }
            else
            {
                // Otherwise add force in the move direction.
                if (m_Rigidbody)
                    m_Rigidbody.AddForce(moveDirection * m_MovePower);
            }
            if (jump && canJump && !hasJumped)
            {
                canJump = false;
                hasJumped = true;
                float jumpX = 0;
                float jumpZ = 0;
                jumpX = Mathf.Clamp(jumpX, 0, 1);
                jumpZ = Mathf.Clamp(jumpZ, 0, 1);
                m_Rigidbody.AddForce(Vector3.up * m_JumpPower, ForceMode.Impulse);
            }
        }

    }
}
