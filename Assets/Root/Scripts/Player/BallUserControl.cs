using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Vehicles.Ball
{
    public class BallUserControl : MonoBehaviour
    {
        public bool canJump;
        private Ball ball; // Reference to the ball controller.

        private Vector3 move;
        // the world-relative desired move direction, calculated from the camForward and user input.

        //private Transform cam; // A reference to the main camera in the scenes transform
        private Vector3 camForward; // The current forward direction of the camera

        public int inputDirection = 0;

        //public Renderer rend;
        //  public Material mat;

        private void Awake()
        {
            // Set up the reference.
            ball = GetComponent<Ball>();

            //rend = transform.GetChild(0).GetComponent<Renderer>();
            // mat = rend.material; 
            // get the transform of the main camera
            if (Camera.main != null)
            {
                // cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Ball needs a Camera tagged \"MainCamera\", for camera-relative controls.");
                // we use world-relative controls in this case, which may not be what the user wants, but hey, we warned them!
            }
        }

        private void Update()
        {
            // Get the axis and jump input.
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
             bool jump = CrossPlatformInputManager.GetButton("Jump");

            if(!canJump)
                ball.jump = false;
            else
                ball.jump = jump;


            Vector3 right = new Vector3(0f, 0f, -1f);
            Vector3 up = new Vector3(1, 0, 0f);
            // calculate camera relative direction to move:
            if (inputDirection == 0)
            {
                right = new Vector3(1f, 0, 0f);
                up = new Vector3(0, 0, 1);
            }
            else
            {
                right = new Vector3(0f, 0f, -1f);
                up = new Vector3(1, 0, 0f);
            }

            Vector3 newTorque = (h * right + v * up);

            //m_Body.AddTorque(newTorque, ForceMode.VelocityChange)
            //move = (v*camForward + h*cam.right).normalized;

            move = newTorque;

            //adjustEmissionIntensity();
        }

        //private void adjustEmissionIntensity()
        //{
        //    float intensity = 1.5f;
        //    if (!target)
        //        return;
        //    var dist = Vector3.Distance(gameObject.transform.position, target.transform.position);
        //    if (dist <= 60 && dist > 35)
        //    {
        //        intensity = Mathf.PingPong(Time.time * 10, 3f);
        //    }
        //    else if (dist <= 35 && dist > 10)
        //    {
        //        intensity = Mathf.PingPong(Time.time * 10, 5f);
        //    }
        //    else if (dist <= 10 && dist > 0)
        //    {
        //        intensity = Mathf.PingPong(Time.time * 20, 8);
        //    }
        //    else
        //    {
        //        intensity = 1f;
        //    }
        //    mat.EnableKeyword("_EMISSION");
        //    mat.SetColor("_EmissionColor", new Color(1f, 0f, 0f, 1.0f) * intensity);
        //}
        private void FixedUpdate()
        {

            /*if (Input.GetKeyDown(KeyCode.P))
             {
                 ball.MakeCircle(1000);
             }
             */
            // Call the Move function of the ball controller
            ball.Move(move);
        }
    }
}
