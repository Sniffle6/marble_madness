﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Ball;

public class DetectDeath : MonoBehaviour
{
    public float deathHeight = 5.3f;
    public GameObject respawnLocation;
    public Vector3 CameraSpawnLocation;
    public GameObject trail;
    public bool isDead = false;
    float value = 0;
    Material m;
    public bool canDie = true;
    // Component halo;
    Rigidbody m_Rigidbody;
    int layerMask = 1 << 8;
    //new Camera camera;
    // GameCamera g_Camera;
    void Start()
    {
        // camera = Camera.main;
        // g_Camera = camera.GetComponent<GameCamera>();
        // CameraSpawnLocation = Camera.main.transform.position;
        m_Rigidbody = GetComponent<Rigidbody>();
        // halo = GetComponent("Halo");
        m = this.transform.Find("ball").GetComponent<Renderer>().material;
        if (!trail)
            trail = gameObject.transform.Find("Trail").gameObject;
        respawnLocation = GameObject.Find("Respawn").gameObject;
    }
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "ImmortalZone")
        {
            canDie = false;
        }
        if (collision.gameObject.tag == "DeathZone")
        {
            Die();
        }
        highestPoint = transform.position;
        distance = 0;
        checkDistance = 0;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "ImmortalZone")
        {
            canDie = true;
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (highestPoint.y - transform.position.y > deathHeight)
        {
            if (GetComponent<Ball>().m_moveSpeed > 5.0f)
            {
                // print("Move speed: " + GetComponent<Ball>().m_moveSpeed);
                // print("Distance: " +( highestPoint.y - transform.position.y) + " : "+ transform.position.y + " : " + highestPoint.y);
                Die();
            }
        }
        if (collision.gameObject.tag == "DeathZone")
        {
            Die();
        }
        highestPoint = transform.position;
        distance = 0;
        checkDistance = 0;
    }
    void Die()
    {

        if (!canDie)
            return;
        StopAllCoroutines();
        StartCoroutine(Respawn(1));
    }
    float distance = 0;
    float checkDistance = 0.0f;
    Vector3 highestPoint = new Vector3();
    Vector3 hitPoint = new Vector3();
    private void Update()
    {
        if (!GetComponent<Ball>().onGround)
        {
            Ray ray = new Ray(transform.position, Vector3.down);
            RaycastHit hit;
            Debug.DrawRay(transform.position, Vector3.down);
            if (Physics.Raycast(ray, out hit, deathHeight + 20, layerMask))
            {
                hitPoint = hit.point;
                checkDistance = transform.position.y - hitPoint.y;
                if (checkDistance > distance)
                {
                    highestPoint = transform.position;
                    distance = checkDistance;
                    // print(hit.collider.name + ":" + hitPoint + ":" + highestPoint + ":" + distance);
                }
            }
        }
    }
    // void Update() 
    //  {
    //  if (isDead /*&& value < 1*/)
    //{
    //value = Mathf.MoveTowards(value, 1, Time.deltaTime);
    // m.SetFloat("_DissolveAmount", value);
    // Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, CameraSpawnLocation, Time.deltaTime * (Vector3.Distance(Camera.main.transform.position, CameraSpawnLocation) / 15));
    // }
    //else if (!isDead && value > -0.1f)
    //{
    //    value = Mathf.MoveTowards(value, 0, Time.deltaTime);
    //    m.SetFloat("_DissolveAmount", value);
    //    if (value < .2f && trail.GetComponent<TrailRenderer>().enabled == false)
    //    {
    //       // halo.GetType().GetProperty("enabled").SetValue(halo, true, null);
    //        trail.GetComponent<TrailRenderer>().enabled = true;
    //    }
    //}
    //  }
    public void InitializeRespawn(float duration)
    {
        StopAllCoroutines();
        StartCoroutine(Respawn(duration));
    }
    IEnumerator Respawn(float timeTillRespawn)
    {
        isDead = true;
        HandleSounds.instance.PlaySound(1);
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        gameObject.transform.GetChild(2).gameObject.SetActive(false);
        GetComponent<SphereCollider>().enabled = false;
        //g_Camera.enabled = false;
        //camera.transform.position = respawnLocation.transform.position+new Vector3(-5f,11f,-5f);
        trail.GetComponent<TrailRenderer>().enabled = false;
        // halo.GetType().GetProperty("enabled").SetValue(halo, false, null);
        respawnLocation = GameObject.Find("Respawn").gameObject;
        //Camera.main.GetComponent<ScreenShake>().InitializeScreenShake(2, 10, 0.5f);

        yield return new WaitForSeconds(timeTillRespawn);    //Wait one frame
        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        gameObject.transform.GetChild(2).gameObject.SetActive(true);
        GetComponent<SphereCollider>().enabled = true;

        m_Rigidbody.velocity = Vector3.zero;
        m_Rigidbody.angularVelocity = Vector3.zero;
        // if(!respawnLocation)
        //     respawnLocation = GameObject.Find("Respawn").gameObject;
        gameObject.transform.position = respawnLocation.transform.position;
        //if(Player.instance.)
        //GameManager.instance.MovePlayerToLastCheckPoint();
        //g_Camera.enabled = true;
        isDead = false;
    }
}
