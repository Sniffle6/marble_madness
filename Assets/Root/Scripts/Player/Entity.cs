﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Entity : MonoBehaviour {
    public List<ItemObject> _ownedItems = new List<ItemObject>();
}
