﻿using UnityEngine;
using System.Collections;

public class ParticleController : MonoBehaviour
{
    #region variables
    public ComputeShader ParticleCalculation;
    public Material ParticleMaterial;
    public int NumParticles = 50000000;
    public float Radius = 10.0f;
    public float StartSpeed = 4.0f;
    public Texture2D HueTexture;

    private const int c_groupSize = 128;
    private int m_updateParticlesKernal;
    #endregion

    #region particleStruct
    //This has to match the Structure in the ParticleCalculation exactly!!!
    struct Particle
    {
        public Vector3 position;
        public Vector3 velocity;
        public Vector3 color;
    }
    #endregion

    #region buffers
    private ComputeBuffer m_particlesBuffer;
    private const int c_particleStride = 36;

    private ComputeBuffer m_quadPoints;
    private const int c_quadStride = 12;

    //might should put these in their own region
    float FramesUntillParticlesStop = 0;
    float modify = 10;
    #endregion

    #region setup
    void Start()
    {
        //find the compute Kernal
        m_updateParticlesKernal = ParticleCalculation.FindKernel("UpdateParticles");

        //create the particle buffer
        m_particlesBuffer = new ComputeBuffer(NumParticles, c_particleStride);

        Particle[] particles = new Particle[NumParticles];

        for (int i = 0; i < NumParticles; i++)
        {
            particles[i].position = gameObject.transform.position  + Random.insideUnitSphere * Radius;
            particles[i].velocity = Random.insideUnitSphere * StartSpeed;
            particles[i].color = Vector3.one;
        }

        m_particlesBuffer.SetData(particles);

        //create quad buffer
        m_quadPoints = new ComputeBuffer(6, c_quadStride);

        m_quadPoints.SetData(new[]
        {
            new Vector3(-0.5f, 0.5f),
            new Vector3(0.5f, 0.5f),
            new Vector3(0.5f, -0.5f),
            new Vector3(0.5f, -0.5f),
            new Vector3(-0.5f, -0.5f),
            new Vector3(-0.5f, 0.5f)
        });
    }
    #endregion

    #region Compute update
    void Update()
    {
        FramesUntillParticlesStop++;
        //bind resources to the ComputeShader
        ParticleCalculation.SetBuffer(m_updateParticlesKernal, "particles", m_particlesBuffer);
        ParticleCalculation.SetFloat("deltaTime", Time.deltaTime);
        ParticleCalculation.SetVector("cameraPosition", Camera.main.transform.position);
        if (FramesUntillParticlesStop < 100)
            ParticleCalculation.SetFloat("speedModify", modify);
        else
        {
            modify = Mathf.Lerp(modify, 0.0015f, Time.deltaTime);
            ParticleCalculation.SetFloat("speedModify", modify);
        }
        ParticleCalculation.SetTexture(m_updateParticlesKernal, "HueTexture", HueTexture);
        //Dispatch launch threads to gpu!
        int numberOfGroups = Mathf.CeilToInt((float)NumParticles / c_groupSize);
        ParticleCalculation.Dispatch(m_updateParticlesKernal, numberOfGroups, 1, 1);

    }
    #endregion

    #region rendering
    void OnRenderObject()
    {
        //bind resources to material
        ParticleMaterial.SetBuffer("particles", m_particlesBuffer);
        ParticleMaterial.SetBuffer("quadPoints", m_quadPoints);

        //set the pass
        ParticleMaterial.SetPass(0);

        //draw
        Graphics.DrawProcedural(MeshTopology.Triangles, 6, NumParticles);
    }
    #endregion
    #region cleanUp
    void OnDestroy()
    {
        m_particlesBuffer.Release();
        m_quadPoints.Release();
    }
    #endregion
}
