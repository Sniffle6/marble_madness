﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Object/Object Modification")]
public class ModifyObjectType : ScriptableObject
{

    public bool m_KillPlayer;
    public bool knockBack;

    Player _player;
    Player player
    {
        get { if (_player) { return _player; } else { return _player = Player.instance; } }
    }
    public void Initialize()
    {

        if (m_KillPlayer)
            Debug.Log("Died");
        else
            Debug.Log("Not Died");

        if (knockBack)
        {
            player.transform.position = player.transform.position - player.transform.forward;
        }

    }
    public void FlipActive(GameObject obj)
    {
        obj.SetActive(!obj.activeSelf);
    }
}
