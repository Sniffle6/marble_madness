﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "New Screen Shake")]
public class ScreenShakeObject : ScriptableObject
{
    public ScreenShakeData screenShakeData;
    Player _player;
    Player player
    {
        get { if (_player) { return _player; } else { return _player = Player.instance; } }
    }
    public void Initialize()
    {
        player.currentScreenShake = screenShakeData;
        if (screenShakeData.normalShake)
            Camera.main.GetComponent<ScreenShake>().normalShake = true;
        if (screenShakeData.projectionShake)
            Camera.main.GetComponent<ScreenShake>().projectionShake = true;
    }
}
