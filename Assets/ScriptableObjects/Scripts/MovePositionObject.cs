﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Object/Static Object Move")]
public class MovePositionObject : ScriptableObject
{
    public bool isLocked;
    public ItemObject key;
    public PistonState state;
    public void Initialize(GameObject obj)
    {
        Debug.Log("Hit");
        if (isLocked)
        {
            if (!Player.instance._ownedItems.Contains(key))
            {
                return;
            }
        }
        var piston = obj.GetComponent<Piston>();
        var states = piston.States;
        bool dontAdd = false;
        for (int i = 0; i < states.Count; i++)
        {
            if (states[i].Name == state.Name)
                dontAdd = true;
        }
        if (!dontAdd)
            piston.States.Add(state);
        obj.GetComponent<PistonLinkedTrigger>().InitializeMove(state.Name);
    }
}
