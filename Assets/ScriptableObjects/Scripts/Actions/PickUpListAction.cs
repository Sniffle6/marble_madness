﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace EntityActions {
    [CreateAssetMenu(menuName="Actions/Pickup Item List")]
    public class PickUpListAction : ActionObject
    {
        public List<ItemObject> _itemsOnObject = new List<ItemObject>();
        /// <summary>
        /// Initialize Pickup Action
        /// </summary>
        /// <param name="obj">The entity that initialized the action</param>
        public override void Initialize()
        {
            var entity = Player.instance.GetComponent<Entity>();
            entity._ownedItems.AddRange(_itemsOnObject);
        }

    }
}
