﻿using UnityEngine;
using System;
namespace EntityActions
{
    [Serializable]
    public abstract class ActionObject : ScriptableObject
    {

        /// <summary>
        /// Initialize Action
        /// </summary>
        public abstract void Initialize();
    }
}