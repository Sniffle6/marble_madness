﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace EntityActions
{
    [CreateAssetMenu(menuName="Actions/Remove Item List")]
    public class RemoveListAction : ActionObject
    {
        public List<ItemObject> _itemsToRemove = new List<ItemObject>();
        public override void Initialize()
        {
            for (int i = 0; i < _itemsToRemove.Count; i++)
            {
                _itemsToRemove[i].Remove();
            }
        }
    }
}

