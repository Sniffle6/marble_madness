﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Actions/Locked Door")]
public class LockRoomAction : ScriptableObject
{
    public ItemObject _requiredKey;
    public MovePositionObject _MoveTooPosition;
    public void Initialize(GameObject lockedDoor)
    {
        if (Player.instance._ownedItems.Remove(_requiredKey))
        {
            _MoveTooPosition.Initialize(lockedDoor);
        }
    }
}
