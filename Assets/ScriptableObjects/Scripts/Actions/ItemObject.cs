﻿using System;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "New Item")]
[Serializable]
public class ItemObject : ScriptableObject {
    public string _name;
    public GameObject _graphics;

    public void Add()
    {
        Player.instance._ownedItems.Add(this);
    }
    public bool Remove()
    {
        if (Player.instance._ownedItems.Remove(this))
            return true;
        else
            return false;
    }

}
