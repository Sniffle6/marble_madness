﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="Actions/Delete")]
public class DeleteObject : ScriptableObject {

	public void Initialize(GameObject obj)
    {

        Destroy(obj);
    }
}
