﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelComplete : MonoBehaviour {
    public int levelToLoad;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            SceneManager.LoadScene(levelToLoad, LoadSceneMode.Single);
        }
	}
    private void OnTriggerEnter(Collider other)
    {
        print("you win");
        SceneManager.LoadScene(levelToLoad, LoadSceneMode.Single);
    }

}
