﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public ItemObject _data;
    // Use this for initialization
    void Start()
    {
        name = _data.name;
        if (transform.childCount == 0)
        {
            var g = Instantiate(_data._graphics);
            g.transform.SetParent(transform);
            g.transform.localPosition = Vector3.zero;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!_data.name.ToLower().Contains("chest"))
            _data.Add();
        Destroy(this.gameObject);
    }
}
